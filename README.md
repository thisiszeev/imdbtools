IMPORTANT!!! READ THIS TOP NOTICE BEFORE DOWNLOADING!

IMDb has a no scraping policy. This was pointed out to me on the
Jellyfin SubReddit. Bear in mind, running this script goes against
IMDb's Terms and Conditions. I intend to leave the original imdbjson.sh
on here for educational purposes only! But there is good news...
   After contacting IMDb for clarification on accessing their metadata
   for this OpenSource project, they kindly pointed me to where I users
   can download the entire IMDb dataset (limited metadata only... but
   fits the requirements of this project) so that it can be processed 
   locally on the PC running the tools that this project intends to
   provide. This is great news, and the datasets are in text format,
   and only take up 4.6GB on the local PC. The .gz files are updated
   daily and appears to contain 9.9 million + titles. Total download
   size is 1.1GB before unpacking the .gz files.

   Special thank you for IMDb!

imdbjson.sh version 0.73

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

This script is used to fetch movies' details from the terminal using IMDb
and generate a JSON file from those details.

This script is part of a personal project where I am building a comprehensive
script that is a collection of various tools for querying IMDb from the Linux
BASH terminal. 

PLEASE NOTE: Myself and this project have no connection to IMDb, other than 
it is a website I use almost daily. The project was born out of a need to
accomplish an automated task via a cronjob. IMDb is a part of Amazon. I also
have no connection to Amazon or any of it's other affliates, other than that
of a consumer. Do not even consider using this project to clone, mass download, 
or abuse IMDb service in any way. Respect their service, and play nice. If
Amazon issue me a takedown notice due to abuse through people using this
project, I will not hesitate to take it down.

My name is Ze'ev Schurmann and you can download the latest version of this
script from https://gitlab.com/thisiszeev/imdbtools 

Todo list:
1. Create a tool imdbshow.sh that displays:
   - Title
   - Release Year
   - Running Time
   - Rating (PG/16/18/R/etc)
   - Genres
   - Running Time
   - Directors
   - Writers
   - Producers
   - Actors
   - Summary

2. Update imdbjson.sh to fix the issue with some movies failing and add functionality to export JSON data for Directors, Writers, Proucers, Actors (This would all work in the same way as their pages are structured the same.)

3. Add --short to imdbjson.sh to allow export of a shortened JSON file based on the metadata listed in point 1.

4. Create a tool imdbnfo.sh that exports an NFO file in the Jellyfin structure as per the metadata listed in point 1.

5. Add --poster to download the poster artwork for the imdbnfo.sh tool and save as png or jpeg.

6. Create a tool imdbsearch.sh that prints all search results for a search string from IMDb.

7. Add --movie_only --cast_only --show_only etc to limit search results as per user requirements.

If you have any further ideas you can contact me on Reddit u/thisiszeev.
