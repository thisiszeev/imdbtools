#!/bin/bash

# imdbjson.sh version 0.73

# IMPORTANT!!! READ THIS TOP NOTICE BEFORE DOWNLOADING!

# IMDb has a no scraping policy. This was pointed out to me on the
# Jellyfin SubReddit. Bear in mind, running this script goes against
# IMDb's Terms and Conditions. I intend to leave the original imdbjson.sh
# on here for educational purposes only! But there is good news...
#    After contacting IMDb for clarification on accessing their metadata
#    for this OpenSource project, they kindly pointed me to where I users
#    can download the entire IMDb dataset (limited metadata only... but
#    fits the requirements of this project) so that it can be processed 
#    locally on the PC running the tools that this project intends to
#    provide. This is great news, and the datasets are in text format,
#    and only take up 4.6GB on the local PC. The .gz files are updated
#    daily and appears to contain 9.9 million + titles. Total download
#    size is 1.1GB before unpacking the .gz files.

#    Special thank you for IMDb!

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This script is used to fetch movies' details from the terminal using IMDb
# and generate a JSON file from those details.

# This script is part of a personal project where I am building a comprehensive
# script that is a collection of various tools for querying IMDb from the Linux
# BASH terminal. 

# PLEASE NOTE: Myself and this project have no connection to IMDb, other than 
# it is a website I use almost daily. The project was born out of a need to
# accomplish an automated task via a cronjob. IMDb is a part of Amazon. I also
# have no connection to Amazon or any of it's other affliates, other than that
# of a consumer. Do not even consider using this project to clone, mass download, 
# or abuse IMDb service in any way. Respect their service, and play nice. If
# Amazon issue me a takedown notice due to abuse through people using this
# project, I will not hesitate to take it down.

# My name is Ze'ev Schurmann and you can download the latest version of this
# script from https://gitlab.com/thisiszeev/imdbtools


# First taking the movie as an argument
if [[ $# -ne 1 ]]; then
  echo "Correct usage is..."
  echo ""
  echo -e "$ ./imdbjson.sh \"MOVIE TITLE YEAR\""
  exit 1
fi
 
# Get the IMDB id
movie_s=${1// /+}  #movie_s=$(echo "$1" | sed 's| |+|g')
echo "Requested: ""$1"
echo "Search String: ""$movie_s"
search_uri="https://www.imdb.com/find?q=$movie_s&s=tt"
echo "Search URI: ""$search_uri"
movie_id=$(curl -s "$search_uri" | grep -Eo 'tt[[:digit:]]{7}' | head -1)
echo "Movie ID: ""$movie_id"
 
# Check if valid
if [[ -z $movie_id ]]; then
    echo -e "Sorry: couldn\'t find the movie.\nIn case of a typo check:\n"
    echo "$1" | aspell -a
    exit 1
fi
 
 
# Parsing
 
# Download title page
movie_uri="https://www.imdb.com/title/"$movie_id
echo "Movie URI: ""$movie_uri"
wget -O "$movie_id".html "$movie_uri"
 
# Check if file exists
if ! [[ -s $movie_id.html ]]; then
    echo -e "Error: couldn\'t get the movie's page."
    exit 1
fi
 
# Export JSON
movie_json=$(grep -Eo '<script id="__NEXT_DATA__" type="application/json">[[:print:]]+</script>' "$movie_id".html | sed 's|<script id="__NEXT_DATA__" type="application/json">||g' | sed 's|</script>||g')
echo "Exporting JSON ""$movie_id"".json"
echo "$movie_json" > "$movie_id".json.tmp
python3 -m json.tool "$movie_id".json.tmp "$movie_id".json || echo "Python3 required!"
rm "$movie_id".html
rm "$movie_id".json.tmp
